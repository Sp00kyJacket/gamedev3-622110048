using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
    public float _horizontalMovementScale=0.5f;
    public float _verticalMovementScale=0.5f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(new Vector3(Input.GetAxis("Horizontal")* _horizontalMovementScale, 0 , Input.GetAxis("Vertical")* _verticalMovementScale));
    }
}
