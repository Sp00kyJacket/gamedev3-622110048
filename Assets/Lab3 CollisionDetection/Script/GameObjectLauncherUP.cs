using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectLauncherUP : MonoBehaviour
{
    [SerializeField]
    protected GameObject _gameObjectToBeLaunched;
    
    [SerializeField]
    protected Transform _launchPosition;
    
    [SerializeField]
    protected float _forceMagnitude;
    
    [SerializeField]
    [Tooltip(tooltip: "Objectlifetimeinseconds")]
    protected float _objectLifeTime;
    
    [SerializeField]
    [Tooltip(tooltip: "Intervalinseconds")]
    protected float _launchInterval = 1.0f;
    
    [SerializeField]
    private bool _activate =false;

    public bool Activate
    {
        get
        {
            return _activate;
        }
        set
        {
            _activate = value;
        }
    }
        
    // Start is called before the first frame update
    void Start()
    {
        Invoke(methodName: "LaunchTheObject", time: _launchInterval);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void LaunchTheObject()
    {
        Invoke(methodName: "LaunchTheObject", time: _launchInterval);
        if(!_activate) return;

        GameObject go = Instantiate(original: _gameObjectToBeLaunched);
        go.SetActive(value: true);

        go.transform.position = _launchPosition.position;
        Rigidbody rb = go.AddComponent<Rigidbody>();
        rb.useGravity = false;
        
        rb.AddForce(force: _launchPosition.up.normalized * _forceMagnitude, mode: ForceMode.Impulse);
        
        Destroy(obj: go, t: _objectLifeTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            _activate = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            _activate = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        
    }
}
