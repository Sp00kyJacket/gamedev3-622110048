using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerComponent : MonoBehaviour
{
    private Vector3 _controlAxis = Vector3.zero;

    private const float MOVEMENT_SCALE_FACTOR = 0.1f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _controlAxis = Vector3.zero;

        _controlAxis.x = Input.GetAxis("Horizontal") * MOVEMENT_SCALE_FACTOR;
        _controlAxis.z = Input.GetAxis("Vertical") * MOVEMENT_SCALE_FACTOR;

        if (Input.GetKey(KeyCode.Home))
        {
            _controlAxis.y += MOVEMENT_SCALE_FACTOR;
        }

        if (Input.GetKey(KeyCode.End))
        {
            _controlAxis.y -= MOVEMENT_SCALE_FACTOR;
        }

        if (this.GetComponent<Rigidbody>() != null)
        {
            this.GetComponent<Rigidbody>().transform.position += _controlAxis;
        }
        else
        {
            this.transform.position += _controlAxis;
        }
    }
}
